const images = document.querySelectorAll('.image-to-show');
const stopButton = document.getElementById('stopBtn');
const resumeButton = document.getElementById('resumeBtn');
let currentIndex = 0;
let intervalId;

function showImage(index) {
  images.forEach((image, i) => {
    if (i === index) {
      image.style.display = 'block';
    } else {
      image.style.display = 'none';
    }
  });
}

function startSlideshow() {
  intervalId = setInterval(() => {
    showImage(currentIndex);
    currentIndex = (currentIndex + 1) % images.length;
  }, 3000);
}

startSlideshow();

stopButton.addEventListener('click', () => {
  clearInterval(intervalId);
});

resumeButton.addEventListener('click', () => {
  startSlideshow();
});